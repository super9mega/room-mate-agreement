\contentsline {section}{\numberline {1}Purpose of Document}{4}{}%
\contentsline {subsection}{\numberline {1.1}foreword}{4}{}%
\contentsline {subsection}{\numberline {1.2}things to note}{4}{}%
\contentsline {section}{\numberline {2}Rent \& Bills}{5}{}%
\contentsline {subsection}{\numberline {2.1}Mortgage}{5}{}%
\contentsline {subsection}{\numberline {2.2}Power \& Gas}{5}{}%
\contentsline {subsection}{\numberline {2.3}Water}{5}{}%
\contentsline {subsection}{\numberline {2.4}Trash}{5}{}%
\contentsline {subsection}{\numberline {2.5}Internet}{5}{}%
\contentsline {section}{\numberline {3}Comfort}{5}{}%
\contentsline {subsection}{\numberline {3.1}Temperature}{5}{}%
\contentsline {subsubsection}{\numberline {3.1.1}Summer}{5}{}%
\contentsline {subsubsection}{\numberline {3.1.2}Winter}{5}{}%
\contentsline {subsubsection}{\numberline {3.1.3}Spring}{5}{}%
\contentsline {subsubsection}{\numberline {3.1.4}Fall}{5}{}%
\contentsline {subsection}{\numberline {3.2}Privacy}{5}{}%
\contentsline {subsection}{\numberline {3.3}Quiet times}{5}{}%
\contentsline {subsection}{\numberline {3.4}Lighting}{5}{}%
\contentsline {subsection}{\numberline {3.5}Moving furniture}{5}{}%
\contentsline {section}{\numberline {4}Guest}{5}{}%
\contentsline {subsection}{\numberline {4.1}Maximum stay}{5}{}%
\contentsline {subsection}{\numberline {4.2}Maximum amount}{5}{}%
\contentsline {subsection}{\numberline {4.3}Messes, responsibility, and restrictions}{6}{}%
\contentsline {subsection}{\numberline {4.4}Additional food}{6}{}%
\contentsline {subsection}{\numberline {4.5}Planning}{7}{}%
\contentsline {section}{\numberline {5}Children}{7}{}%
\contentsline {section}{\numberline {6}Pets}{7}{}%
\contentsline {subsection}{\numberline {6.1}Food Bowls}{7}{}%
\contentsline {subsection}{\numberline {6.2}Loudness}{7}{}%
\contentsline {subsection}{\numberline {6.3}Damage}{7}{}%
\contentsline {subsection}{\numberline {6.4}Hair}{7}{}%
\contentsline {subsection}{\numberline {6.5}Bathing}{7}{}%
\contentsline {section}{\numberline {7}Chores}{7}{}%
\contentsline {subsection}{\numberline {7.1}Daily}{7}{}%
\contentsline {subsubsection}{\numberline {7.1.1}Cooking}{7}{}%
\contentsline {subsubsection}{\numberline {7.1.2}Dishes}{8}{}%
\contentsline {subsubsection}{\numberline {7.1.3}vacuuming}{8}{}%
\contentsline {subsubsection}{\numberline {7.1.4}moping}{8}{}%
\contentsline {subsubsection}{\numberline {7.1.5}Dusting \& Cleaning surfaces}{8}{}%
\contentsline {subsubsection}{\numberline {7.1.6}Feeding Pets}{8}{}%
\contentsline {subsubsection}{\numberline {7.1.7}Trash}{8}{}%
\contentsline {subsection}{\numberline {7.2}Weekly}{8}{}%
\contentsline {subsubsection}{\numberline {7.2.1}Bathroom}{8}{}%
\contentsline {subsubsection}{\numberline {7.2.2}Mowing}{8}{}%
\contentsline {subsubsection}{\numberline {7.2.3}Gardening}{8}{}%
\contentsline {subsubsection}{\numberline {7.2.4}Laundry}{8}{}%
\contentsline {subsubsection}{\numberline {7.2.5}Cleaning out fridge}{8}{}%
\contentsline {subsection}{\numberline {7.3}Monthly}{8}{}%
\contentsline {subsubsection}{\numberline {7.3.1}Air filters}{8}{}%
\contentsline {subsubsection}{\numberline {7.3.2}Washing Windows}{8}{}%
\contentsline {section}{\numberline {8}Leaving early}{8}{}%
\contentsline {section}{\numberline {9}Food \& Kitchen}{8}{}%
\contentsline {subsection}{\numberline {9.1}Sharing Food}{8}{}%
\contentsline {subsection}{\numberline {9.2}Cooking}{8}{}%
\contentsline {subsubsection}{\numberline {9.2.1}Schedule}{8}{}%
\contentsline {subsection}{\numberline {9.3}Trash}{8}{}%
\contentsline {subsection}{\numberline {9.4}Dishwasher}{8}{}%
\contentsline {subsection}{\numberline {9.5}Sitting dishes}{8}{}%
\contentsline {subsection}{\numberline {9.6}Large messes}{9}{}%
\contentsline {subsection}{\numberline {9.7}appliance failure}{9}{}%
\contentsline {section}{\numberline {10}Parking}{9}{}%
\contentsline {subsection}{\numberline {10.1}garage}{9}{}%
\contentsline {section}{\numberline {11}Fires (Fire Pit)}{9}{}%
\contentsline {section}{\numberline {12}Emergency}{9}{}%
\contentsline {subsection}{\numberline {12.1}Fire}{9}{}%
\contentsline {subsection}{\numberline {12.2}Flooding}{9}{}%
\contentsline {subsection}{\numberline {12.3}Snow}{9}{}%
\contentsline {subsection}{\numberline {12.4}Wind}{9}{}%
\contentsline {subsection}{\numberline {12.5}Infestation}{9}{}%
\contentsline {subsection}{\numberline {12.6}Tree}{9}{}%
\contentsline {subsection}{\numberline {12.7}Car Crash}{9}{}%
\contentsline {section}{\numberline {13}Signing}{10}{}%
